// //Soal 1

var nilai = 75;

if (nilai >= 92) {
    console.log("A")
} else if (nilai >= 75 && nilai < 85){
    console.log("B")
} else if (nilai >= 65 && nilai < 75){
    console.log("C")
} else if (nilai >= 55 && nilai < 65){
    console.log("D")
} else {
    console.log("E")
}


//Soal 2 

var tanggal = 08;
var tahun = 2002;
var bulan = 5;

switch(bulan){
    case 1: {console.log('bukan TTL saya '); break;}
    case 2: {console.log('bukan TTL saya juga'); break;}
    default: {console.log('8 Mei 2002');}
}


//Soal 3

function segitiga(panjang) {
    let hasil = ''
    for (let i = 0; i < panjang; i++) {
        for (let j = 0; j <= i; j++) {
            hasil = hasil + '# '
        }
        hasil = hasil + '\n'
    }
    return hasil;
}
console.log(segitiga(3));


//SOal 4

const kata = ["Programming", "Javascript", "VueJS"]

let panjang = kata.length
let index = 0
let nomor = 1
masukan = 10
while (nomor <= masukan) {
  console.log(nomor + " - I love " + kata[index])
  index += 1
  nomor += 1

  if (index == panjang) {
    console.log("=".repeat(nomor - 1))
    index = 0
  }
}
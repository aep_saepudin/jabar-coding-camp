//SOAL 1

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var kata1 = pertama.substring(0,5)
var kata2 = pertama.substring(12,18)
var kata3 = kedua.substring(0,7)
var kata4 = kedua.substring(7,19).toUpperCase()

console.log(kata1 + kata2 + " " + kata3 + kata4) // saya senang belajar JAVASCRIPT

//SOAL 2

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var angka1 = Number(kataPertama);
var angka2 = Number(kataKedua)
var angka3 = Number(kataKetiga)
var angka4 = Number(kataKeempat)

var hasilAngka = (angka1 % angka2) + (angka3 * angka4);

console.log(hasilAngka) // 24